package com.entity.view;

import com.entity.ShenqingweixiuEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import java.io.Serializable;
import com.utils.EncryptUtil;
 

/**
 * 申请维修
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@TableName("shenqingweixiu")
public class ShenqingweixiuView  extends ShenqingweixiuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public ShenqingweixiuView(){
	}
 
 	public ShenqingweixiuView(ShenqingweixiuEntity shenqingweixiuEntity){
 	try {
			BeanUtils.copyProperties(this, shenqingweixiuEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}


}
