package com.entity.view;

import com.entity.WeixiufankuiEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import java.io.Serializable;
import com.utils.EncryptUtil;
 

/**
 * 维修反馈
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@TableName("weixiufankui")
public class WeixiufankuiView  extends WeixiufankuiEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public WeixiufankuiView(){
	}
 
 	public WeixiufankuiView(WeixiufankuiEntity weixiufankuiEntity){
 	try {
			BeanUtils.copyProperties(this, weixiufankuiEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}


}
