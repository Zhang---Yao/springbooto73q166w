package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.ZulinqiandingEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.ZulinqiandingVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.ZulinqiandingView;


/**
 * 租赁签订
 *
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ZulinqiandingService extends IService<ZulinqiandingEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ZulinqiandingVO> selectListVO(Wrapper<ZulinqiandingEntity> wrapper);
   	
   	ZulinqiandingVO selectVO(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);
   	
   	List<ZulinqiandingView> selectListView(Wrapper<ZulinqiandingEntity> wrapper);
   	
   	ZulinqiandingView selectView(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ZulinqiandingEntity> wrapper);

   	

    List<Map<String, Object>> selectValue(Map<String, Object> params,Wrapper<ZulinqiandingEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params,Wrapper<ZulinqiandingEntity> wrapper);

    List<Map<String, Object>> selectGroup(Map<String, Object> params,Wrapper<ZulinqiandingEntity> wrapper);



}

