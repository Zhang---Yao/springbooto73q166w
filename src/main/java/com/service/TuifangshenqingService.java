package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.TuifangshenqingEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.TuifangshenqingVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.TuifangshenqingView;


/**
 * 退房申请
 *
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface TuifangshenqingService extends IService<TuifangshenqingEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<TuifangshenqingVO> selectListVO(Wrapper<TuifangshenqingEntity> wrapper);
   	
   	TuifangshenqingVO selectVO(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);
   	
   	List<TuifangshenqingView> selectListView(Wrapper<TuifangshenqingEntity> wrapper);
   	
   	TuifangshenqingView selectView(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<TuifangshenqingEntity> wrapper);

   	

}

