package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.ShenqingkanfangEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.ShenqingkanfangVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.ShenqingkanfangView;


/**
 * 申请看房
 *
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ShenqingkanfangService extends IService<ShenqingkanfangEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ShenqingkanfangVO> selectListVO(Wrapper<ShenqingkanfangEntity> wrapper);
   	
   	ShenqingkanfangVO selectVO(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);
   	
   	List<ShenqingkanfangView> selectListView(Wrapper<ShenqingkanfangEntity> wrapper);
   	
   	ShenqingkanfangView selectView(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ShenqingkanfangEntity> wrapper);

   	

}

