package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.ShenqingweixiuEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.ShenqingweixiuVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.ShenqingweixiuView;


/**
 * 申请维修
 *
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ShenqingweixiuService extends IService<ShenqingweixiuEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<ShenqingweixiuVO> selectListVO(Wrapper<ShenqingweixiuEntity> wrapper);
   	
   	ShenqingweixiuVO selectVO(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);
   	
   	List<ShenqingweixiuView> selectListView(Wrapper<ShenqingweixiuEntity> wrapper);
   	
   	ShenqingweixiuView selectView(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<ShenqingweixiuEntity> wrapper);

   	

}

