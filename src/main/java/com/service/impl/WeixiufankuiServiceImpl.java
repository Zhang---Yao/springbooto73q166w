package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.WeixiufankuiDao;
import com.entity.WeixiufankuiEntity;
import com.service.WeixiufankuiService;
import com.entity.vo.WeixiufankuiVO;
import com.entity.view.WeixiufankuiView;

@Service("weixiufankuiService")
public class WeixiufankuiServiceImpl extends ServiceImpl<WeixiufankuiDao, WeixiufankuiEntity> implements WeixiufankuiService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<WeixiufankuiEntity> page = this.selectPage(
                new Query<WeixiufankuiEntity>(params).getPage(),
                new EntityWrapper<WeixiufankuiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<WeixiufankuiEntity> wrapper) {
		  Page<WeixiufankuiView> page =new Query<WeixiufankuiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

    
    @Override
	public List<WeixiufankuiVO> selectListVO(Wrapper<WeixiufankuiEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public WeixiufankuiVO selectVO(Wrapper<WeixiufankuiEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<WeixiufankuiView> selectListView(Wrapper<WeixiufankuiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public WeixiufankuiView selectView(Wrapper<WeixiufankuiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
