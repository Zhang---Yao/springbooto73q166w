package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.TuifangshenqingDao;
import com.entity.TuifangshenqingEntity;
import com.service.TuifangshenqingService;
import com.entity.vo.TuifangshenqingVO;
import com.entity.view.TuifangshenqingView;

@Service("tuifangshenqingService")
public class TuifangshenqingServiceImpl extends ServiceImpl<TuifangshenqingDao, TuifangshenqingEntity> implements TuifangshenqingService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<TuifangshenqingEntity> page = this.selectPage(
                new Query<TuifangshenqingEntity>(params).getPage(),
                new EntityWrapper<TuifangshenqingEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<TuifangshenqingEntity> wrapper) {
		  Page<TuifangshenqingView> page =new Query<TuifangshenqingView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

    
    @Override
	public List<TuifangshenqingVO> selectListVO(Wrapper<TuifangshenqingEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public TuifangshenqingVO selectVO(Wrapper<TuifangshenqingEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<TuifangshenqingView> selectListView(Wrapper<TuifangshenqingEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public TuifangshenqingView selectView(Wrapper<TuifangshenqingEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
