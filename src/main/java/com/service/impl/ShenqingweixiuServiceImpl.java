package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.ShenqingweixiuDao;
import com.entity.ShenqingweixiuEntity;
import com.service.ShenqingweixiuService;
import com.entity.vo.ShenqingweixiuVO;
import com.entity.view.ShenqingweixiuView;

@Service("shenqingweixiuService")
public class ShenqingweixiuServiceImpl extends ServiceImpl<ShenqingweixiuDao, ShenqingweixiuEntity> implements ShenqingweixiuService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ShenqingweixiuEntity> page = this.selectPage(
                new Query<ShenqingweixiuEntity>(params).getPage(),
                new EntityWrapper<ShenqingweixiuEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ShenqingweixiuEntity> wrapper) {
		  Page<ShenqingweixiuView> page =new Query<ShenqingweixiuView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

    
    @Override
	public List<ShenqingweixiuVO> selectListVO(Wrapper<ShenqingweixiuEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ShenqingweixiuVO selectVO(Wrapper<ShenqingweixiuEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ShenqingweixiuView> selectListView(Wrapper<ShenqingweixiuEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ShenqingweixiuView selectView(Wrapper<ShenqingweixiuEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
