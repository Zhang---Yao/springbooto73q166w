package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.ZulinqiandingDao;
import com.entity.ZulinqiandingEntity;
import com.service.ZulinqiandingService;
import com.entity.vo.ZulinqiandingVO;
import com.entity.view.ZulinqiandingView;

@Service("zulinqiandingService")
public class ZulinqiandingServiceImpl extends ServiceImpl<ZulinqiandingDao, ZulinqiandingEntity> implements ZulinqiandingService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ZulinqiandingEntity> page = this.selectPage(
                new Query<ZulinqiandingEntity>(params).getPage(),
                new EntityWrapper<ZulinqiandingEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ZulinqiandingEntity> wrapper) {
		  Page<ZulinqiandingView> page =new Query<ZulinqiandingView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

    
    @Override
	public List<ZulinqiandingVO> selectListVO(Wrapper<ZulinqiandingEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ZulinqiandingVO selectVO(Wrapper<ZulinqiandingEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ZulinqiandingView> selectListView(Wrapper<ZulinqiandingEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ZulinqiandingView selectView(Wrapper<ZulinqiandingEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    @Override
    public List<Map<String, Object>> selectValue(Map<String, Object> params, Wrapper<ZulinqiandingEntity> wrapper) {
        return baseMapper.selectValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params, Wrapper<ZulinqiandingEntity> wrapper) {
        return baseMapper.selectTimeStatValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectGroup(Map<String, Object> params, Wrapper<ZulinqiandingEntity> wrapper) {
        return baseMapper.selectGroup(params, wrapper);
    }




}
