package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.ShenqingkanfangDao;
import com.entity.ShenqingkanfangEntity;
import com.service.ShenqingkanfangService;
import com.entity.vo.ShenqingkanfangVO;
import com.entity.view.ShenqingkanfangView;

@Service("shenqingkanfangService")
public class ShenqingkanfangServiceImpl extends ServiceImpl<ShenqingkanfangDao, ShenqingkanfangEntity> implements ShenqingkanfangService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ShenqingkanfangEntity> page = this.selectPage(
                new Query<ShenqingkanfangEntity>(params).getPage(),
                new EntityWrapper<ShenqingkanfangEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<ShenqingkanfangEntity> wrapper) {
		  Page<ShenqingkanfangView> page =new Query<ShenqingkanfangView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

    
    @Override
	public List<ShenqingkanfangVO> selectListVO(Wrapper<ShenqingkanfangEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public ShenqingkanfangVO selectVO(Wrapper<ShenqingkanfangEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<ShenqingkanfangView> selectListView(Wrapper<ShenqingkanfangEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public ShenqingkanfangView selectView(Wrapper<ShenqingkanfangEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
