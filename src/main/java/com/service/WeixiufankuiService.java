package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.WeixiufankuiEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.WeixiufankuiVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.WeixiufankuiView;


/**
 * 维修反馈
 *
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface WeixiufankuiService extends IService<WeixiufankuiEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<WeixiufankuiVO> selectListVO(Wrapper<WeixiufankuiEntity> wrapper);
   	
   	WeixiufankuiVO selectVO(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);
   	
   	List<WeixiufankuiView> selectListView(Wrapper<WeixiufankuiEntity> wrapper);
   	
   	WeixiufankuiView selectView(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<WeixiufankuiEntity> wrapper);

   	

}

