package com.dao;

import com.entity.ShenqingweixiuEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.ShenqingweixiuVO;
import com.entity.view.ShenqingweixiuView;


/**
 * 申请维修
 * 
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ShenqingweixiuDao extends BaseMapper<ShenqingweixiuEntity> {
	
	List<ShenqingweixiuVO> selectListVO(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);
	
	ShenqingweixiuVO selectVO(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);
	
	List<ShenqingweixiuView> selectListView(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);

	List<ShenqingweixiuView> selectListView(Pagination page,@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);

	
	ShenqingweixiuView selectView(@Param("ew") Wrapper<ShenqingweixiuEntity> wrapper);
	

}
