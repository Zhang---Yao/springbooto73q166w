package com.dao;

import com.entity.ZulinqiandingEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.ZulinqiandingVO;
import com.entity.view.ZulinqiandingView;


/**
 * 租赁签订
 * 
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ZulinqiandingDao extends BaseMapper<ZulinqiandingEntity> {
	
	List<ZulinqiandingVO> selectListVO(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);
	
	ZulinqiandingVO selectVO(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);
	
	List<ZulinqiandingView> selectListView(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);

	List<ZulinqiandingView> selectListView(Pagination page,@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);

	
	ZulinqiandingView selectView(@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);
	

    List<Map<String, Object>> selectValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);

    List<Map<String, Object>> selectGroup(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<ZulinqiandingEntity> wrapper);



}
