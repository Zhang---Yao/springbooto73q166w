package com.dao;

import com.entity.ShenqingkanfangEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.ShenqingkanfangVO;
import com.entity.view.ShenqingkanfangView;


/**
 * 申请看房
 * 
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface ShenqingkanfangDao extends BaseMapper<ShenqingkanfangEntity> {
	
	List<ShenqingkanfangVO> selectListVO(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);
	
	ShenqingkanfangVO selectVO(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);
	
	List<ShenqingkanfangView> selectListView(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);

	List<ShenqingkanfangView> selectListView(Pagination page,@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);

	
	ShenqingkanfangView selectView(@Param("ew") Wrapper<ShenqingkanfangEntity> wrapper);
	

}
