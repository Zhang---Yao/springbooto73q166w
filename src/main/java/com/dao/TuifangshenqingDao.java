package com.dao;

import com.entity.TuifangshenqingEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.TuifangshenqingVO;
import com.entity.view.TuifangshenqingView;


/**
 * 退房申请
 * 
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface TuifangshenqingDao extends BaseMapper<TuifangshenqingEntity> {
	
	List<TuifangshenqingVO> selectListVO(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);
	
	TuifangshenqingVO selectVO(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);
	
	List<TuifangshenqingView> selectListView(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);

	List<TuifangshenqingView> selectListView(Pagination page,@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);

	
	TuifangshenqingView selectView(@Param("ew") Wrapper<TuifangshenqingEntity> wrapper);
	

}
