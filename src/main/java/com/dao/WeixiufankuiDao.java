package com.dao;

import com.entity.WeixiufankuiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.WeixiufankuiVO;
import com.entity.view.WeixiufankuiView;


/**
 * 维修反馈
 * 
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
public interface WeixiufankuiDao extends BaseMapper<WeixiufankuiEntity> {
	
	List<WeixiufankuiVO> selectListVO(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);
	
	WeixiufankuiVO selectVO(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);
	
	List<WeixiufankuiView> selectListView(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);

	List<WeixiufankuiView> selectListView(Pagination page,@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);

	
	WeixiufankuiView selectView(@Param("ew") Wrapper<WeixiufankuiEntity> wrapper);
	

}
