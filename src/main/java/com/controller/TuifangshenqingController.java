package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.TuifangshenqingEntity;
import com.entity.view.TuifangshenqingView;

import com.service.TuifangshenqingService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;
import com.utils.MapUtils;
import com.utils.CommonUtil;
import java.io.IOException;

/**
 * 退房申请
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@RestController
@RequestMapping("/tuifangshenqing")
public class TuifangshenqingController {
    @Autowired
    private TuifangshenqingService tuifangshenqingService;




    



    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,TuifangshenqingEntity tuifangshenqing,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			tuifangshenqing.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("fangdong")) {
			tuifangshenqing.setFangdongzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<TuifangshenqingEntity> ew = new EntityWrapper<TuifangshenqingEntity>();

		PageUtils page = tuifangshenqingService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, tuifangshenqing), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,TuifangshenqingEntity tuifangshenqing, 
		HttpServletRequest request){
        EntityWrapper<TuifangshenqingEntity> ew = new EntityWrapper<TuifangshenqingEntity>();

		PageUtils page = tuifangshenqingService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, tuifangshenqing), params), params));
        return R.ok().put("data", page);
    }



	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( TuifangshenqingEntity tuifangshenqing){
       	EntityWrapper<TuifangshenqingEntity> ew = new EntityWrapper<TuifangshenqingEntity>();
      	ew.allEq(MPUtil.allEQMapPre( tuifangshenqing, "tuifangshenqing")); 
        return R.ok().put("data", tuifangshenqingService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(TuifangshenqingEntity tuifangshenqing){
        EntityWrapper< TuifangshenqingEntity> ew = new EntityWrapper< TuifangshenqingEntity>();
 		ew.allEq(MPUtil.allEQMapPre( tuifangshenqing, "tuifangshenqing")); 
		TuifangshenqingView tuifangshenqingView =  tuifangshenqingService.selectView(ew);
		return R.ok("查询退房申请成功").put("data", tuifangshenqingView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        TuifangshenqingEntity tuifangshenqing = tuifangshenqingService.selectById(id);
        return R.ok().put("data", tuifangshenqing);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        TuifangshenqingEntity tuifangshenqing = tuifangshenqingService.selectById(id);
        return R.ok().put("data", tuifangshenqing);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody TuifangshenqingEntity tuifangshenqing, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(tuifangshenqing);
        tuifangshenqingService.insert(tuifangshenqing);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody TuifangshenqingEntity tuifangshenqing, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(tuifangshenqing);
        tuifangshenqingService.insert(tuifangshenqing);
        return R.ok();
    }





    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody TuifangshenqingEntity tuifangshenqing, HttpServletRequest request){
        //ValidatorUtils.validateEntity(tuifangshenqing);
        tuifangshenqingService.updateById(tuifangshenqing);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<TuifangshenqingEntity> list = new ArrayList<TuifangshenqingEntity>();
        for(Long id : ids) {
            TuifangshenqingEntity tuifangshenqing = tuifangshenqingService.selectById(id);
            tuifangshenqing.setSfsh(sfsh);
            tuifangshenqing.setShhf(shhf);
            list.add(tuifangshenqing);
        }
        tuifangshenqingService.updateBatchById(list);
        return R.ok();
    }


    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        tuifangshenqingService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	










}
