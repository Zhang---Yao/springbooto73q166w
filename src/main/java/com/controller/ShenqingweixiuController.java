package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.ShenqingweixiuEntity;
import com.entity.view.ShenqingweixiuView;

import com.service.ShenqingweixiuService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;
import com.utils.MapUtils;
import com.utils.CommonUtil;
import java.io.IOException;

/**
 * 申请维修
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@RestController
@RequestMapping("/shenqingweixiu")
public class ShenqingweixiuController {
    @Autowired
    private ShenqingweixiuService shenqingweixiuService;




    



    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,ShenqingweixiuEntity shenqingweixiu,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			shenqingweixiu.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("fangdong")) {
			shenqingweixiu.setFangdongzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<ShenqingweixiuEntity> ew = new EntityWrapper<ShenqingweixiuEntity>();

		PageUtils page = shenqingweixiuService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, shenqingweixiu), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,ShenqingweixiuEntity shenqingweixiu, 
		HttpServletRequest request){
        EntityWrapper<ShenqingweixiuEntity> ew = new EntityWrapper<ShenqingweixiuEntity>();

		PageUtils page = shenqingweixiuService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, shenqingweixiu), params), params));
        return R.ok().put("data", page);
    }



	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( ShenqingweixiuEntity shenqingweixiu){
       	EntityWrapper<ShenqingweixiuEntity> ew = new EntityWrapper<ShenqingweixiuEntity>();
      	ew.allEq(MPUtil.allEQMapPre( shenqingweixiu, "shenqingweixiu")); 
        return R.ok().put("data", shenqingweixiuService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(ShenqingweixiuEntity shenqingweixiu){
        EntityWrapper< ShenqingweixiuEntity> ew = new EntityWrapper< ShenqingweixiuEntity>();
 		ew.allEq(MPUtil.allEQMapPre( shenqingweixiu, "shenqingweixiu")); 
		ShenqingweixiuView shenqingweixiuView =  shenqingweixiuService.selectView(ew);
		return R.ok("查询申请维修成功").put("data", shenqingweixiuView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        ShenqingweixiuEntity shenqingweixiu = shenqingweixiuService.selectById(id);
        return R.ok().put("data", shenqingweixiu);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        ShenqingweixiuEntity shenqingweixiu = shenqingweixiuService.selectById(id);
        return R.ok().put("data", shenqingweixiu);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ShenqingweixiuEntity shenqingweixiu, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(shenqingweixiu);
        shenqingweixiuService.insert(shenqingweixiu);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody ShenqingweixiuEntity shenqingweixiu, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(shenqingweixiu);
        shenqingweixiuService.insert(shenqingweixiu);
        return R.ok();
    }





    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody ShenqingweixiuEntity shenqingweixiu, HttpServletRequest request){
        //ValidatorUtils.validateEntity(shenqingweixiu);
        shenqingweixiuService.updateById(shenqingweixiu);//全部更新
        return R.ok();
    }



    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        shenqingweixiuService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	










}
