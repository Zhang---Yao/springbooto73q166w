package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.ShenqingkanfangEntity;
import com.entity.view.ShenqingkanfangView;

import com.service.ShenqingkanfangService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;
import com.utils.MapUtils;
import com.utils.CommonUtil;
import java.io.IOException;

/**
 * 申请看房
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@RestController
@RequestMapping("/shenqingkanfang")
public class ShenqingkanfangController {
    @Autowired
    private ShenqingkanfangService shenqingkanfangService;




    



    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,ShenqingkanfangEntity shenqingkanfang,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("fangdong")) {
			shenqingkanfang.setFangdongzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("yonghu")) {
			shenqingkanfang.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<ShenqingkanfangEntity> ew = new EntityWrapper<ShenqingkanfangEntity>();

		PageUtils page = shenqingkanfangService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, shenqingkanfang), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,ShenqingkanfangEntity shenqingkanfang, 
		HttpServletRequest request){
        EntityWrapper<ShenqingkanfangEntity> ew = new EntityWrapper<ShenqingkanfangEntity>();

		PageUtils page = shenqingkanfangService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, shenqingkanfang), params), params));
        return R.ok().put("data", page);
    }



	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( ShenqingkanfangEntity shenqingkanfang){
       	EntityWrapper<ShenqingkanfangEntity> ew = new EntityWrapper<ShenqingkanfangEntity>();
      	ew.allEq(MPUtil.allEQMapPre( shenqingkanfang, "shenqingkanfang")); 
        return R.ok().put("data", shenqingkanfangService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(ShenqingkanfangEntity shenqingkanfang){
        EntityWrapper< ShenqingkanfangEntity> ew = new EntityWrapper< ShenqingkanfangEntity>();
 		ew.allEq(MPUtil.allEQMapPre( shenqingkanfang, "shenqingkanfang")); 
		ShenqingkanfangView shenqingkanfangView =  shenqingkanfangService.selectView(ew);
		return R.ok("查询申请看房成功").put("data", shenqingkanfangView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        ShenqingkanfangEntity shenqingkanfang = shenqingkanfangService.selectById(id);
        return R.ok().put("data", shenqingkanfang);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        ShenqingkanfangEntity shenqingkanfang = shenqingkanfangService.selectById(id);
        return R.ok().put("data", shenqingkanfang);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ShenqingkanfangEntity shenqingkanfang, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(shenqingkanfang);
        shenqingkanfangService.insert(shenqingkanfang);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody ShenqingkanfangEntity shenqingkanfang, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(shenqingkanfang);
        shenqingkanfangService.insert(shenqingkanfang);
        return R.ok();
    }





    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody ShenqingkanfangEntity shenqingkanfang, HttpServletRequest request){
        //ValidatorUtils.validateEntity(shenqingkanfang);
        shenqingkanfangService.updateById(shenqingkanfang);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<ShenqingkanfangEntity> list = new ArrayList<ShenqingkanfangEntity>();
        for(Long id : ids) {
            ShenqingkanfangEntity shenqingkanfang = shenqingkanfangService.selectById(id);
            shenqingkanfang.setSfsh(sfsh);
            shenqingkanfang.setShhf(shhf);
            list.add(shenqingkanfang);
        }
        shenqingkanfangService.updateBatchById(list);
        return R.ok();
    }


    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        shenqingkanfangService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	










}
