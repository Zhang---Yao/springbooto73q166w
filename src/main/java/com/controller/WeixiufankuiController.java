package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.WeixiufankuiEntity;
import com.entity.view.WeixiufankuiView;

import com.service.WeixiufankuiService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;
import com.utils.MapUtils;
import com.utils.CommonUtil;
import java.io.IOException;

/**
 * 维修反馈
 * 后端接口
 * @author 
 * @email 
 * @date 2024-03-10 18:36:01
 */
@RestController
@RequestMapping("/weixiufankui")
public class WeixiufankuiController {
    @Autowired
    private WeixiufankuiService weixiufankuiService;




    



    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,WeixiufankuiEntity weixiufankui,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			weixiufankui.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("fangdong")) {
			weixiufankui.setFangdongzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<WeixiufankuiEntity> ew = new EntityWrapper<WeixiufankuiEntity>();

		PageUtils page = weixiufankuiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, weixiufankui), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,WeixiufankuiEntity weixiufankui, 
		HttpServletRequest request){
        EntityWrapper<WeixiufankuiEntity> ew = new EntityWrapper<WeixiufankuiEntity>();

		PageUtils page = weixiufankuiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, weixiufankui), params), params));
        return R.ok().put("data", page);
    }



	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( WeixiufankuiEntity weixiufankui){
       	EntityWrapper<WeixiufankuiEntity> ew = new EntityWrapper<WeixiufankuiEntity>();
      	ew.allEq(MPUtil.allEQMapPre( weixiufankui, "weixiufankui")); 
        return R.ok().put("data", weixiufankuiService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(WeixiufankuiEntity weixiufankui){
        EntityWrapper< WeixiufankuiEntity> ew = new EntityWrapper< WeixiufankuiEntity>();
 		ew.allEq(MPUtil.allEQMapPre( weixiufankui, "weixiufankui")); 
		WeixiufankuiView weixiufankuiView =  weixiufankuiService.selectView(ew);
		return R.ok("查询维修反馈成功").put("data", weixiufankuiView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        WeixiufankuiEntity weixiufankui = weixiufankuiService.selectById(id);
        return R.ok().put("data", weixiufankui);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        WeixiufankuiEntity weixiufankui = weixiufankuiService.selectById(id);
        return R.ok().put("data", weixiufankui);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WeixiufankuiEntity weixiufankui, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(weixiufankui);
        weixiufankuiService.insert(weixiufankui);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody WeixiufankuiEntity weixiufankui, HttpServletRequest request){
    	//ValidatorUtils.validateEntity(weixiufankui);
        weixiufankuiService.insert(weixiufankui);
        return R.ok();
    }





    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody WeixiufankuiEntity weixiufankui, HttpServletRequest request){
        //ValidatorUtils.validateEntity(weixiufankui);
        weixiufankuiService.updateById(weixiufankui);//全部更新
        return R.ok();
    }



    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        weixiufankuiService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	










}
