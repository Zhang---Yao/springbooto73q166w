import Vue from 'vue';
//配置路由
import VueRouter from 'vue-router'
Vue.use(VueRouter);
//1.创建组件
import Index from '@/views/index'
import Home from '@/views/home'
import Login from '@/views/login'
import NotFound from '@/views/404'
import UpdatePassword from '@/views/update-password'
import pay from '@/views/pay'
import register from '@/views/register'
import center from '@/views/center'
    import fangwuleixing from '@/views/modules/fangwuleixing/list'
    import discussfangwuxinxi from '@/views/modules/discussfangwuxinxi/list'
    import news from '@/views/modules/news/list'
    import aboutus from '@/views/modules/aboutus/list'
    import zulinqianding from '@/views/modules/zulinqianding/list'
    import storeup from '@/views/modules/storeup/list'
    import fangwuxinxi from '@/views/modules/fangwuxinxi/list'
    import shenqingweixiu from '@/views/modules/shenqingweixiu/list'
    import forum from '@/views/modules/forum/list'
    import tuifangshenqing from '@/views/modules/tuifangshenqing/list'
    import systemintro from '@/views/modules/systemintro/list'
    import jiaofeijilu from '@/views/modules/jiaofeijilu/list'
    import yonghu from '@/views/modules/yonghu/list'
    import weixiufankui from '@/views/modules/weixiufankui/list'
    import fangdong from '@/views/modules/fangdong/list'
    import config from '@/views/modules/config/list'
    import shenqingkanfang from '@/views/modules/shenqingkanfang/list'
    import newstype from '@/views/modules/newstype/list'


//2.配置路由   注意：名字
export const routes = [{
    path: '/',
    name: '系统首页',
    component: Index,
    children: [{
      // 这里不设置值，是把main作为默认页面
      path: '/',
      name: '系统首页',
      component: Home,
      meta: {icon:'', title:'center', affix: true}
    }, {
      path: '/updatePassword',
      name: '修改密码',
      component: UpdatePassword,
      meta: {icon:'', title:'updatePassword'}
    }, {
      path: '/pay',
      name: '支付',
      component: pay,
      meta: {icon:'', title:'pay'}
    }, {
      path: '/center',
      name: '个人信息',
      component: center,
      meta: {icon:'', title:'center'}
    }
      ,{
	path: '/fangwuleixing',
        name: '房屋类型',
        component: fangwuleixing
      }
      ,{
	path: '/discussfangwuxinxi',
        name: '房屋信息评论',
        component: discussfangwuxinxi
      }
      ,{
	path: '/news',
        name: '公告信息',
        component: news
      }
      ,{
	path: '/aboutus',
        name: '关于我们',
        component: aboutus
      }
      ,{
	path: '/zulinqianding',
        name: '租赁签订',
        component: zulinqianding
      }
      ,{
	path: '/storeup',
        name: '我的收藏',
        component: storeup
      }
      ,{
	path: '/fangwuxinxi',
        name: '房屋信息',
        component: fangwuxinxi
      }
      ,{
	path: '/shenqingweixiu',
        name: '申请维修',
        component: shenqingweixiu
      }
      ,{
	path: '/forum',
        name: '交流论坛',
        component: forum
      }
      ,{
	path: '/tuifangshenqing',
        name: '退房申请',
        component: tuifangshenqing
      }
      ,{
	path: '/systemintro',
        name: '系统简介',
        component: systemintro
      }
      ,{
	path: '/jiaofeijilu',
        name: '缴费记录',
        component: jiaofeijilu
      }
      ,{
	path: '/yonghu',
        name: '用户',
        component: yonghu
      }
      ,{
	path: '/weixiufankui',
        name: '维修反馈',
        component: weixiufankui
      }
      ,{
	path: '/fangdong',
        name: '房东',
        component: fangdong
      }
      ,{
	path: '/config',
        name: '轮播图管理',
        component: config
      }
      ,{
	path: '/shenqingkanfang',
        name: '申请看房',
        component: shenqingkanfang
      }
      ,{
	path: '/newstype',
        name: '公告信息分类',
        component: newstype
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {icon:'', title:'login'}
  },
  {
    path: '/register',
    name: 'register',
    component: register,
    meta: {icon:'', title:'register'}
  },
  {
    path: '*',
    component: NotFound
  }
]
//3.实例化VueRouter  注意：名字
const router = new VueRouter({
  mode: 'hash',
  /*hash模式改为history*/
  routes // （缩写）相当于 routes: routes
})
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}
export default router;
