const base = {
    get() {
        return {
            url : "http://localhost:8080/springbooto73q166w/",
            name: "springbooto73q166w",
            // 退出到首页链接
            indexUrl: 'http://localhost:8080/springbooto73q166w/front/h5/index.html'
        };
    },
    getProjectName(){
        return {
            projectName: "公寓租赁管理小程序设计与实现"
        } 
    }
}
export default base
